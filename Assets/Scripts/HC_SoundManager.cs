﻿///////////////////////////////////////////////////////////
//
// HC_SoundManager ver 1.0.0
//
// created by.Jumpei Tamai 
// since.2016.07.20
//


using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class HC_SoundManager : MonoBehaviour {

    //SEのチャンネル数.
    const int SE_CHANNEL = 5;

    //サウンド種別.
    enum Type
    {
        Bgm,
        Se,
    }

    static HC_SoundManager _singleton = null;

    public static HC_SoundManager GetInstance()
    {
        return _singleton ?? (_singleton = new HC_SoundManager());
    }

    //サウンド再生のためのゲームオブジェクト.
    GameObject _object = null;
    //サウンドリソース.
    AudioSource _sourceBgm = null;
    AudioSource _sourceSeDefault = null;
    AudioSource[] _sourceSeArray;

    Dictionary<string, _Data> _poolBgm = new Dictionary<string, _Data>();
    Dictionary<string, _Data> _poolSe = new Dictionary<string, _Data>();
    

    /// 保持するデータ
    class _Data
    {
        /// アクセス用のキー
        public string Key;
        /// リソース名
        public string ResName;
        /// AudioClip
        public AudioClip Clip;

        /// コンストラクタ
        public _Data(string key, string res)
        {
            Key = key;
            ResName = "Sounds/" + res;
            // AudioClipの取得
            Clip = Resources.Load(ResName) as AudioClip;
        }
    }

    public HC_SoundManager()
    {
        //チャンネル確保.
        _sourceSeArray = new AudioSource[SE_CHANNEL];
    }

    //AudiouSource取得
    AudioSource _GetAudioSource(Type type, int channel = -1)
    {
        if(_object == null)
        {
            _object = new GameObject("Sound");
            GameObject.DontDestroyOnLoad(_object);
            _sourceBgm = _object.AddComponent<AudioSource>();
            _sourceSeDefault = _object.AddComponent<AudioSource>();
            for(int i = 0;i<SE_CHANNEL;i++)
            {
                _sourceSeArray[i] = _object.AddComponent<AudioSource>();
            }
        }

        if(type == Type.Bgm)
        {
            //BGM.
            return _sourceBgm;
        }
        else
        {
            //SE.
            if(0 <= channel && channel < SE_CHANNEL)
            {
                //チャンネル.
                return _sourceSeArray[channel];
            }
            else
            {   //デフォルト.
                return _sourceSeDefault;
            }
        }
    }

    //サウンドのロード
    /*音楽は　Resource/Soundsフォルダに配置　*/
    public static void LoadBgm(string key, string resName)
    {
        GetInstance()._LoadBgm(key, resName);
    }

    public static void LoadSe(string key, string resName)
    {
        GetInstance()._LoadSe(key, resName);
    }


    void _LoadBgm(string key, string resName)
    {
        //すでに登録されていないかチェック
        if(_poolBgm.ContainsKey(key))
        {
            //すでに登録済みなので一旦消す
            _poolBgm.Remove(key);
        }
        _poolBgm.Add(key, new _Data(key, resName));
    }

    void _LoadSe(string key, string resName)
    {
        //すでに登録されていないかチェック
        if (_poolSe.ContainsKey(key))
        {
            //すでに登録済みなので一旦消す
            _poolSe.Remove(key);
        }
        _poolSe.Add(key, new _Data(key, resName));
    }

    //音楽の再生.
    //事前に読み込むこと！
    public static bool PlayBgm(string key)
    {
        return GetInstance()._PlayBgm(key);
    }

    bool _PlayBgm(string key)
    {
        if(_poolBgm.ContainsKey(key) == false)
        {
            return false;
        }

        //一旦止める
        StopBgm();

        //リソースの取得
        var _data = _poolBgm[key];

        //再生
        var source = _GetAudioSource(Type.Bgm);
        source.loop = true;
        source.clip = _data.Clip;
        source.Play();

        return true;
    }

    public static bool StopBgm()
    {
        return GetInstance()._StopBgm();
    }

    bool _StopBgm()
    {
        _GetAudioSource(Type.Bgm).Stop();
        return true;
    }

    public static bool PlaySe(string key, int channel = -1)
    {
        return GetInstance()._PlaySe(key, channel);
    }
    bool _PlaySe(string key, int channel = -1)
    {
        if (_poolSe.ContainsKey(key) == false)
        {
            // 対応するキーがない
            return false;
        }

        // リソースの取得
        var _data = _poolSe[key];

        if (0 <= channel && channel < SE_CHANNEL)
        {
            // チャンネル指定
            var source = _GetAudioSource(Type.Se, channel);
            source.clip = _data.Clip;
            source.Play();
        }
        else
        {
            // デフォルトで再生
            var source = _GetAudioSource(Type.Se);
            source.PlayOneShot(_data.Clip);
        }

        return true;
    }




}
