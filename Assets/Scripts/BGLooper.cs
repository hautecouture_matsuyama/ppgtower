﻿using UnityEngine;
using System.Collections;


public class BGLooper : MonoBehaviour {
	
	private Score sco;

	private int score100;

	private int Randomnum = 25;
	//Instantiate enemy and plan
	
	private Transform player;		// Reference to the player.

    //ケミカルX
	public GameObject[] Props;

	public GameObject Fire;
	
	int numBGPanels = 10;
	
	float pipeMax = 2;
	float pipeMin = -2;

    public static float potionProb = 0.9f;

	
	void Start() {

		//sco = GameObject.FindGameObjectWithTag ("Player").GetComponent<Score> ();
		
		player = GameObject.FindGameObjectWithTag("Player").transform;
		GameObject[] pipes = GameObject.FindGameObjectsWithTag("Pipe");

		foreach(GameObject pipe in pipes) {
			Vector3 pos = pipe.transform.position;
			pos.x = Random.Range(pipeMin, pipeMax);
			pipe.transform.position = pos;
		}
	}
	void Update () {
		
		transform.position = new Vector3(transform.position.x, player.position.y + 11f,0);
		score100 = Mathf.RoundToInt (Score.score / 50);

		if(score100 > 15){
			score100 = 15;
		}

		Randomnum = 25 - score100;
	}
	
	void OnTriggerEnter2D(Collider2D collider) {
		// plan loop
		
		if(collider.tag == "Pipe"){

			float widthOfBGObject = ((BoxCollider2D)collider).size.y;
			
			Vector3 pos = collider.transform.position;
			
			pos.y -= widthOfBGObject * numBGPanels;
			
			if(collider.tag == "Pipe") {
				pos.x = Random.Range(pipeMin, pipeMax);
			}

			collider.transform.position = pos;
			

			if(Random.Range(0,Randomnum) == 5)
			{
				Instantiate(this.Fire,new Vector3(transform.position.x + Random.Range(3,-3), pos.y + widthOfBGObject*2+widthOfBGObject/2, 0), Fire.transform.rotation);
			}
			
			// ケミカルX（クスリ）の出現率
            if (Random.value > 0.5f)
            {
                if (Random.value >= potionProb && Props[0])
                {
                    print("Props");
                    Instantiate(Props[0], new Vector3(transform.position.x + Random.Range(3, -3), pos.y + widthOfBGObject / 3, 0), Quaternion.identity);
                }
            }

           
			
		}
	}

}
