﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SherePlugins : MonoBehaviour {

    public Texture2D shareTexture;

    public Score score;

    [SerializeField]
    private string sharelink;

    void Awake()
    {
#if UNITY_ANDROID
        sharelink = "https://play.google.com/store/apps/details?id=com.hautecouture.ppgtower&hl=ja";
#elif UNITY_IPHONE
        sharelink = "https://itunes.apple.com/us/app/pawapafugaruzu-dokidoki-da/id1174244573?l=ja&ls=1&mt=8";
#endif
    }
    


    public void ShareTwitter()
    {

		UM_ShareUtility.TwitterShare("パワーパフガールズ ドキドキ大脱出で\n " + score.scoreUI.text + "点獲得したよ！\n " + sharelink, shareTexture);
    }

    public void ShareLINE()
    {
		string msg = "パワーパフガールズ ドキドキ大脱出で"+score.scoreUI.text+"点獲得したよ！\n "+sharelink;
        string url = "http://line.me/R/msg/text/?" + WWW.EscapeURL(msg);
        Application.OpenURL(url);
    }

    public void ShareFacebook()
    {
        if (!FB.IsInitialized)
        {
            FB.Init(Login, null, null);
            return;
        }
        else
        {
            Login();
        }
    }

    void Login()
    {
        if (!FB.IsLoggedIn)
        {
            FB.Login("", LoginCallback);
            return;
        }
        else
        {
            LoginCallback();
        }
    }

    string FeedLink = "";
    string FeedLinkName = "パワーパフガールズ ドキドキ大脱出";
    string FeedLinkDescription = "";
    string FeedPicture = "";
    private Dictionary<string, string[]> FeedProperties = new Dictionary<string, string[]>();

    void LoginCallback(FBResult result = null)
    {
        if (FB.IsLoggedIn)
        {
            FeedLink = sharelink;


            FB.Feed(
                link: FeedLink,
                linkName: FeedLinkName,
                linkDescription: FeedLinkDescription,
				picture: "https://100apps.s3.amazonaws.com/000_Original/ShareImage/ppg_tower_header.jpg",
                properties: FeedProperties
            );
        }
    }

    public void OnClick()
    {
        if (gameObject.name == "share_facebook")
        {
            ShareFacebook();
        }

        if (gameObject.name == "share_line")
        {
            ShareLINE();
        }

        if (gameObject.name == "share_twitter")
        {
            ShareTwitter();
        }
    }
}