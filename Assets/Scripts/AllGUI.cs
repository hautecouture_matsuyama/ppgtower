﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class AllGUI : MonoBehaviour {


	public GameObject startButton;
	public GameObject pauseButton;

	public GameObject BGM;

	public bool PauseFlg;

	PlayerControl playercontrol;

    [SerializeField]
    dealLine deal;

    [SerializeField]
    private GameObject countPanel;

    public bool countFlg;

    public static bool planBonus;

	//public Plane Box;

	void Start()
	{
		if(PlayerPrefs.GetString("Init") != "true"){

			PlayerPrefs.SetString("isGravity","false");
			PlayerPrefs.SetInt("level",1);

			PlayerPrefs.SetString("Init","true");

		}

	}

	public void startBotton(){
        //nendAdIcon.Hide();
        //NendManager.Instance.NendBannerHide();
        NendManager.Instance.NendIconHide();

		Application.LoadLevel("Level1");
	}

	public void tiltFun(){
		PlayerPrefs.SetString("isGravity","ture");
	}

	public void touchFun(){
		PlayerPrefs.SetString("isGravity","false");
	}
	public void level1Fun(){
		PlayerPrefs.SetInt("level",1);
		print ("level1");
	}
	public void level2Fun(){
		PlayerPrefs.SetInt("level",2);
		print ("level2");
	}

	public void pauseFun(){
		
		pauseButton.SetActive (false);
		startButton.SetActive (true);
		Time.timeScale = 0;

		NendManager.Instance.NendBannerHide ();
		NendManager.Instance.NendPauseRecShow ();

	}

	public void StartFun(){
		print("sStarts");
		pauseButton.SetActive (true);
		startButton.SetActive (false);

		NendManager.Instance.NendBannerShow ();
		NendManager.Instance.NendPauseRecHide ();
        NendManager.Instance.NendIconHide();
        Time.timeScale = 1;
		this.PauseFlg = false;

        Debug.Log("RESART");
	}




	public void PauseFun(){
	
		pauseButton.SetActive (false);
		startButton.SetActive (true);

		Time.timeScale = 0;

		PauseFlg = true;
  
	}

	public void BackFun(){
		Application.LoadLevel("Start");

    }

	public void RestartFun(){
		Application.LoadLevel(Application.loadedLevel);
        NendManager.Instance.NendIconHide();
        Time.timeScale = 1;
	}

    public void CountDownFun()
    {
        StartCoroutine(CountDown());
    }

    IEnumerator CountDown()
    {
        deal.enabled = false;
        countPanel.SetActive(true);
        countFlg = true;

        yield return new WaitForSeconds(2f);
        countFlg = false;
        countPanel.SetActive(false);

        yield return new WaitForSeconds(0.8f);

        deal.enabled = true;
    }


    public void OtherGames()
    {
		Application.OpenURL("https://itunes.apple.com/us/developer/hautecouture-inc./id901721933");
    }

}
