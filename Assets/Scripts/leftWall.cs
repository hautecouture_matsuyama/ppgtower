﻿using UnityEngine;
using System.Collections;

public class leftWall : MonoBehaviour {



	[SerializeField]
	private GameObject player;

	[SerializeField]
	private Camera cameraO;

	[SerializeField]
	private BoxCollider2D collider;


	void Start()
	{
		if(gameObject.name == "leftWall"){
			//transform.position = new Vector3(cameraO.GetComponent<Camera>().aspect*-10, player.position.y,-1);
			transform.position = new Vector3(cameraO.ViewportToWorldPoint(Vector2.zero).x - collider.size.x / 2, player.transform.position.y,-1);
		}else if(gameObject.name == "rightWall"){
			//transform.position = new Vector3(cameraO.GetComponent<Camera>().aspect*10, player.position.y,-1);
			transform.position = new Vector3(cameraO.ViewportToWorldPoint(Vector2.one).x + collider.size.x / 2, player.transform.position.y, -1);
		}
	}
}
