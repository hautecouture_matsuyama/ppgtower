﻿using UnityEngine;
using System.Collections;

public class dealLine : MonoBehaviour {

    [SerializeField]
	private GameObject player;

	private int num;

    public float farLength = 30;
    public float farSpace = 28;
    public float midMoveSpeed = 4.25f;

    [SerializeField]
	Score scoreIn;

	// Update is called once per frame
	void Update () {

		float dist = (player.transform.position - transform.position).sqrMagnitude;

			if(player){

				if(dist < farLength * farLength){
					transform.Translate(Vector3.down * midMoveSpeed* Time.deltaTime/1.2f);
				}

                else
                {
                    transform.Translate(Vector3.down * midMoveSpeed * 4f * Time.deltaTime );
                }

			}
	
	}
	
}
