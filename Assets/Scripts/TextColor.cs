﻿using UnityEngine;
using System.Collections;

public class TextColor : MonoBehaviour {

	public GUIText[] text;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	

		if(PlayerPrefs.GetString("isGravity") == "false"){
			text[0].GetComponent<GUIText>().color = Color.black;
			text[1].GetComponent<GUIText>().color = Color.red;
		}else{
			text[0].GetComponent<GUIText>().color = Color.red;
			text[1].GetComponent<GUIText>().color = Color.black;
		}

		if(PlayerPrefs.GetInt("level") == 1){
			text[3].GetComponent<GUIText>().color = Color.black;
			text[2].GetComponent<GUIText>().color = Color.red;
		}else{
			text[3].GetComponent<GUIText>().color = Color.red;
			text[2].GetComponent<GUIText>().color = Color.black;
		}

	}
}
