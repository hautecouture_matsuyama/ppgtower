﻿using UnityEngine;
using System.Collections;

public class FollowPlayer : MonoBehaviour
{
	[SerializeField]
	private GameObject player;		// Reference to the player.
	public int Cz;

	void Update ()
	{
		transform.position = new Vector3(transform.position.x, player.transform.position.y,Cz);

	}
}
