﻿using UnityEngine;
using System.Collections;

public class enemyAi : MonoBehaviour {

	[HideInInspector]
	public bool facingRight = true;			// For determining which way the player is currently facing.

	private const int normal=0;
	private const int Run_left=1;
	private const int Run_right=2;

	private float maxSpeed = 3;

	private int state;

	private float aiThankLastTime; 

	private Animator anim;	

	void Start ()
	{
		state = normal;
		anim = GetComponent<Animator>();
	}
	
	void Update ()
	{

		updateEnemyType1();

		if(Application.loadedLevelName == "Start"){

		}else{
			Destroy(this.gameObject,10f);
		}


	}
	void Flip ()
	{
		// Switch the way the player is labelled as facing.
		facingRight = !facingRight;
		
		// Multiply the player's x local scale by -1.
		Vector3 theScale = transform.localScale;
		theScale.x *= -1;
		transform.localScale = theScale;
	}
	
	//更新第二种敌人的AI
	void updateEnemyType1()
	{
		
		//判断敌人是否开始思考
		if(isAIthank())
		{
			//敌人开始思考
			AIthankEnemyState(3);
		}else
		{
			//更新敌人状态
			UpdateEmenyState();
		}
	}
	
	int getRandom(int count)
	{
		
		return new System.Random().Next(count);
		
	}
	
	bool isAIthank()
	{
		//这里表示敌人每3秒进行一次思考
		if(Time.time - aiThankLastTime >= 1f)
		{
			aiThankLastTime = Time.time;
			return true;
			
		}
		return false;
	}
	
	//敌人在这里进行思考
	void AIthankEnemyState(int count)
	{
		//开始随机数字。
		int d = getRandom(count);
		
		switch(d)
		{
		case 0:
			//设置敌人为站立状态
			setEmemyState(normal);
			break;
		case 1:
			//设置敌人为站立状态
			setEmemyState(Run_left);
			break;
		case 2:
			//设置敌人为旋转状态
			setEmemyState(Run_right);
			break;
	
		}
		
	}
	
	void setEmemyState(int newState)
	{
		if(state == newState)
			return;
		state = newState;
	
		switch(state)
		{
		case Run_left:

			break;
		case Run_right:

			break;
	
		}
		
		//避免重复播放动画，这里进行判断
	}
	
	//在这里更新敌人状态
	void UpdateEmenyState()
	{

		switch(state)
		{
		case Run_left:

			if(!facingRight)
				// ... flip the player.
				Flip();

			if(GetComponent<Rigidbody2D>().velocity.x < maxSpeed)
				// ... add a force to the player.
				GetComponent<Rigidbody2D>().AddForce(Vector2.right * -100);
			
			// If the player's horizontal velocity is greater than the maxSpeed...
			if(Mathf.Abs(GetComponent<Rigidbody2D>().velocity.x) > maxSpeed)
				// ... set the player's velocity to the maxSpeed in the x axis.
				GetComponent<Rigidbody2D>().velocity = new Vector2(Mathf.Sign(GetComponent<Rigidbody2D>().velocity.x) * maxSpeed, GetComponent<Rigidbody2D>().velocity.y);


//			print("-right");
			//旋转状态时 敌人开始旋转， 旋转时间为1秒 这样更加具有惯性
			break;
		case Run_right:
			if(facingRight)
				// ... flip the player.
				Flip();
			if(GetComponent<Rigidbody2D>().velocity.x < maxSpeed)
				// ... add a force to the player.
				GetComponent<Rigidbody2D>().AddForce(Vector2.right * 100);
			
			// If the player's horizontal velocity is greater than the maxSpeed...
			if(Mathf.Abs(GetComponent<Rigidbody2D>().velocity.x) > maxSpeed)
				// ... set the player's velocity to the maxSpeed in the x axis.
				GetComponent<Rigidbody2D>().velocity = new Vector2(Mathf.Sign(GetComponent<Rigidbody2D>().velocity.x) * maxSpeed, GetComponent<Rigidbody2D>().velocity.y);

//			print("right");
			break;
	
		}
		
	}

	void OnTriggerEnter2D(Collider2D collider) {

	


		if (collider.name == "Protection") {
		
			GetComponent<AudioSource>().Play();
			anim.SetBool("isdeal",true);

			Destroy(gameObject.GetComponent<Collider2D>());

		}

	}

}