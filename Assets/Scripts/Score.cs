﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Score : MonoBehaviour {
	

	//private int[] arr=new int[]{45,12,44,4,5};
	[HideInInspector]
	public static int score;
	public Text scoreUI;

    [SerializeField]
    private GameObject sub_Playr_Bubbles;

    [SerializeField]
    private GameObject sub_Player_Buttercup;


	void OnTriggerEnter2D(Collider2D other) 
	{
        //得点加算
		if(other.tag == "score")
		{
			score++;

			if(score > PlayerPrefs.GetInt("score"))
			{
				PlayerPrefs.SetInt("score",score);
			}

            if(score ==  10)
            {
                Vector2 pos = new Vector2(-10 * Mathf.Sign(this.gameObject.transform.position.x),this.transform.position.y);
                Instantiate(sub_Playr_Bubbles,pos,Quaternion.identity);

            }
            else if(score == 20)
            {
                Vector2 pos = new Vector2(-10 * Mathf.Sign(this.gameObject.transform.position.x), this.transform.position.y);
                Instantiate(sub_Player_Buttercup, pos, Quaternion.identity);
            }

			if(scoreUI){
				scoreUI.text = "" + score;
			}

			
		}



	}
}
