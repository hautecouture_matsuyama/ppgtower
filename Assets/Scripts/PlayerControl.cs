
using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using NendUnityPlugin.AD;



public class PlayerControl : MonoBehaviour
{
	private float i;
	[HideInInspector]
	public bool facingRight = true;			// For determining which way the player is currently facing.
	[HideInInspector]
	public bool jump = false;				// Condition for whether the player should jump.

	
	public GameObject BoxGUI;
	public GameObject PauseButton;

	public float moveForce = 365f;			// Amount of force added to move the player left and right.
	public float maxSpeed = 3.5f;				// The fastest the player can travel in the x axis.
	public float jumpForce = 1000f;			// Amount of force added when the player jumps.
	public AudioClip[] taunts;				// Array of clips for when the player taunts.
	public float tauntProbability = 50f;	// Chance of a taunt happening.
	public float tauntDelay = 1f;			// Delay for when the taunt should happen.

    public float threshold = .6f;
    public Vector2 boundForce = new Vector2(50, -3);

	private int tauntIndex;					// The index of the taunts array indicating the most recent taunt.
	private Transform groundCheck;			// A position marking where to check if the player is grounded.
	private bool grounded = false;			// Whether or not the player is grounded.

    [SerializeField]
	private TrailRenderer trailrender;      //
	
	[HideInInspector]
	public bool isDeal;   // is deal ?
	
	public AudioClip[] audioClip;     //

	public GameObject Protection;     //Protection 
	private bool isProtection;

	AllGUI allgui;

    [SerializeField]
    private GameObject bgPanel;

    [SerializeField]
    private FollowPlayer followPlayer;

    [SerializeField]
    private Text bonusMessagePlay;

    [SerializeField]
    private GameObject[] bonusMessageImage;

    [SerializeField]
    private GameObject[] bonusImage;


    [SerializeField]
    private Text bonusMessage;

    [SerializeField]
    private GameObject scoreobj;


    static float strongTime;

    [SerializeField]
    private Rigidbody2D rb;

    [SerializeField]
    private Animator animator;

    [SerializeField]
    private SpriteRenderer sp;

    //画面がタッチされたか判定するフラグ
    private bool isTouch;
    //タッチしている画面の座標
    private float touchX;

	public  bool aliveflg;

	[SerializeField]
	private Text myRank;

	[SerializeField]
	private AudioSource audio;

	[SerializeField]
	private CircleCollider2D colli;



	void Start()
	{
		allgui = GameObject.Find ("ButtonManager").GetComponent<AllGUI> ();

        allgui.CountDownFun();

		aliveflg = true;
	}

	void Awake()
	{

		Protection.SetActive (false);
		trailrender.enabled = false;                      //

        NendManager.Instance.NendBannerShow();

        StartCoroutine(StartMessage());

		Application.targetFrameRate = 60; 
		Resources.UnloadUnusedAssets();
	}

    IEnumerator StartMessage()
    {
        yield return new WaitForSeconds(3f);
        //ボーナスメッセージ表示
        StartBonusMessage();
    }


    void StartBonusMessage()
    {
        if (BonusManager.bonusidx != 0)
        {
            bonusMessagePlay.gameObject.SetActive(true);

            switch (BonusManager.bonusidx)
            {
                case 1:
                    //bonusMessagePlay.text = "アイテム出現率上昇中！";
                    bonusMessageImage[0].SetActive(true);
                    break;
                case 2:
                    //bonusMessagePlay.text = "敵出現率減少中！";
                    bonusMessageImage[1].SetActive(true);
                    break;
                case 3:
                    //bonusMessagePlay.text = "足場減少中！";
                    bonusMessageImage[2].SetActive(true);
                    break;

            }

        }

    }

    void Update()
    {
        if (Input.touchCount > 0)
        {
            isTouch = true;
            touchX = Input.GetTouch(0).position.x;
        }
            
        else
            isTouch = false;
    }

    void FixedUpdate()
    {
        if (!allgui.countFlg)
        {
            if (isDeal)
            {
                Protection.SetActive(false);
            }
            else
            {
                if (Application.isEditor || Application.isWebPlayer)
                {

                    // Cache the horizontal input.
                    float h = Input.GetAxis("Horizontal");

                    //			print(h);

                    // The Speed animator parameter is set to the absolute value of the horizontal input.
					//animator.SetFloat("Speed", Mathf.Abs(h));

                    if (isProtection)
                    {

                        print("isProtection");

                        if (h * rb.velocity.x < maxSpeed)
                            // ... add a force to the player.
                            rb.AddForce(Vector2.right * h * moveForce * 5);

                    }
                    else
                    {
                        if (h * rb.velocity.x < maxSpeed)
                            // ... add a force to the player.
                            rb.AddForce(Vector2.right * h * moveForce);
                    }

                    // If the player is changing direction (h has a different sign to velocity.x) or hasn't reached maxSpeed yet...


                    // If the player's horizontal velocity is greater than the maxSpeed...
                    if (Mathf.Abs(rb.velocity.x) > maxSpeed)
                        // ... set the player's velocity to the maxSpeed in the x axis.
                        rb.velocity = new Vector2(Mathf.Sign(rb.velocity.x) * maxSpeed, rb.velocity.y);

                    // If the input is moving the player right and the player is facing left...
                    if (h > 0 && !facingRight)
                        // ... flip the player.
                        Flip();

                    // Otherwise if the input is moving the player left and the player is facing right...
                    else if (h < 0 && facingRight)
                        // ... flip the player.
                        Flip();

                }
                else
                {

                    if (PlayerPrefs.GetString("isGravity") == "ture")
                    {  //判断是重力感应还是手指触摸

						//animator.SetFloat("Speed", Mathf.Abs(Input.acceleration.x));

                        if (isProtection)
                        {

                            if (Input.acceleration.x * rb.velocity.x < maxSpeed)
                                // ... add a force to the player.
                                rb.AddForce(Vector2.right * Input.acceleration.x * moveForce * 3);

                        }
                        else
                        {
                            if (Input.acceleration.x * rb.velocity.x < maxSpeed)
                                // ... add a force to the player.
                                rb.AddForce(Vector2.right * Input.acceleration.x * moveForce);
                        }



                        // If the player's horizontal velocity is greater than the maxSpeed...
                        if (Mathf.Abs(rb.velocity.x) > maxSpeed)
                            // ... set the player's velocity to the maxSpeed in the x axis.
                            rb.velocity = new Vector2(Mathf.Sign(rb.velocity.x) * maxSpeed, rb.velocity.y);

                        // If the input is moving the player right and the player is facing left...
                        if (Input.acceleration.x > 0 && !facingRight)
                            // ... flip the player.
                            Flip();

                        // Otherwise if the input is moving the player left and the player is facing right...
                        else if (Input.acceleration.x < 0 && facingRight)
                            // ... flip the player.
                            Flip();

                    }
                    else
                    {

                        if (isTouch)//如果存在Touch事件  
                        {
                            if (touchX > Screen.width / 2)
                            {
                                if (i < 0.99f)
                                {

                                    i += 0.1f;
                                }
                            }
                            else
                            {
                                if (i > -0.99f)
                                {

                                    i -= 0.1f;
                                }
                            }
                        }
                        else
                        {
                            i = 0;
                        }

						//animator.SetFloat("Speed", Mathf.Abs(i));
                        // If the player is changing direction (h has a different sign to velocity.x) or hasn't reached maxSpeed yet...

                        if (isProtection)
                        {

                            if (i * rb.velocity.x < maxSpeed)
                                // ... add a force to the player.
                                rb.AddForce(Vector2.right * i * moveForce * 5);

                        }
                        else
                        {
                            if (i * rb.velocity.x < maxSpeed)
                                // ... add a force to the player.
                                rb.AddForce(Vector2.right * i * moveForce);
                        }



                        // If the player's horizontal velocity is greater than the maxSpeed...
                        if (Mathf.Abs(rb.velocity.x) > maxSpeed)
                            // ... set the player's velocity to the maxSpeed in the x axis.
                            rb.velocity = new Vector2(Mathf.Sign(rb.velocity.x) * maxSpeed, rb.velocity.y);

                        // If the input is moving the player right and the player is facing left...
                        if (i > 0 && !facingRight)
                            // ... flip the player.
                            Flip();

                        // Otherwise if the input is moving the player left and the player is facing right...
                        else if (i < 0 && facingRight)
                            // ... flip the player.
                            Flip();

                    }

                }
            }

        }
    }
	
	
	void Flip ()
	{
		// Switch the way the player is labelled as facing.
		facingRight = !facingRight;
		
		// Multiply the player's x local scale by -1.
		Vector3 theScale = transform.localScale;
		theScale.x *= -1;
		transform.localScale = theScale;
	}

	public IEnumerator Taunt()
	{
		// Check the random chance of taunting.
		float tauntChance = Random.Range(0f, 100f);
		if(tauntChance > tauntProbability)
		{
			// Wait for tauntDelay number of seconds.
			yield return new WaitForSeconds(tauntDelay);
			
			// If there is no clip currently playing.
			if(!GetComponent<AudioSource>().isPlaying)
			{
				// Choose a random, but different taunt.
				tauntIndex = TauntRandom();
				
				// Play the new taunt.
				audio.clip = taunts[tauntIndex];
				audio.Play();
			}
		}
	}
	
	
	int TauntRandom()
	{
		// Choose a random index of the taunts array.
		int i = Random.Range(0, taunts.Length);
		
		// If it's the same as the previous taunt...
		if(i == tauntIndex)
			// ... try another random taunt.
			return TauntRandom();
		else
			// Otherwise return this index.
			return i;
	}
	void OnCollisionEnter2D(Collision2D coll) {
		
		if (isDeal || isProtection) {
			
		} else {
			if(coll.collider.tag == "Enemy")
			{
				//animator.SetBool("Deal",true);
				isDeal = true;
                Destroy(coll.collider.gameObject);
				StartCoroutine("GameOver");
			}	
		}	
	}

    
	void OnTriggerEnter2D(Collider2D collider) {
		if (isDeal) {
		} else {
			if(collider.name == "dealLine"){
				//print("enemy dal");
				isDeal = true;
				Resources.UnloadUnusedAssets();

				StartCoroutine("GameOver");
			}
			
            //ケミカルXとった時
			if(collider.tag == "Strong"){
                
                //初めて取得
                if (!Protection.activeSelf)
                {
                    strongTime = 10f;
                    trailrender.enabled = true;
                    Follow.enableFlg = true;
                    animator.enabled = true;
                    Destroy(collider.gameObject);
                    StartCoroutine("trailrenderFalse");
                    Sound.Instance.StartSound(2);
                }
                //無敵状態で取得した時
                else
                {
                    Destroy(collider.gameObject);
                    strongTime = 10f;
                    StopCoroutine("trailrenderFalse");
                    StartCoroutine("trailrenderFalse");
                    Debug.Log("TIME" + strongTime);
                }

			}
		}

        if (collider.gameObject.tag == "Enemy")
        {
            if (Protection.activeSelf) return;
            isDeal = true;
            StartCoroutine("GameOver");
        }	
	}

	IEnumerator trailrenderFalse(){
		isProtection = true;
        
		GetComponent<AudioSource>().clip = audioClip[1];
		GetComponent<AudioSource>().Play();

		Protection.SetActive (true);

		yield return new WaitForSeconds(strongTime);

		isProtection = false;
        Follow.enableFlg = false;
        trailrender.enabled = false;
        animator.enabled = false;
        sp.sprite = null;
		GetComponent<AudioSource>().clip = audioClip[2];
		
		GetComponent<AudioSource>().Play();

        Sound.Instance.StartSound(3);

		Protection.SetActive (false);
		
	}


    IEnumerator JumpStop()
    {
        yield return new WaitForSeconds(0.1f);
        rb.isKinematic = false;

        rb.velocity = Vector2.zero;
        //this.gameObject.transform.Rotate(0, 0, -180);
        Vector2 forward = transform.TransformDirection(Vector3.up) * 600f;

        rb.AddForce(forward);
        Destroy(this.gameObject.GetComponent<CircleCollider2D>());

		aliveflg = false;

    }


    void InitBonus()
    {
        BGLooper.potionProb = 0.9f;
        CameraScript.enemyBonus = false;
        AllGUI.planBonus = false;
        BonusManager.bonusidx = 0;
    }

    //ゲームオーバー時のコルーチン
	IEnumerator GameOver(){
		Debug.Log ("gameovereeeeeee");
		audio.clip = audioClip [0];
		audio.loop = false;
		audio.Play ();

		scoreobj.SetActive (false);

		followPlayer.enabled = false;
        
		PlayerPrefs.SetInt ("nowScore", Score.score);
        
        if(Score.score > PlayerPrefs.GetInt("bestscore"))
        {
            RankingManager.Instance.SendScore(Score.score);
            PlayerPrefs.SetInt("bestscore", Score.score);
        }
        
		PauseButton.SetActive (false);

		StartCoroutine (JumpStop ());

		bgPanel.SetActive (true);
		InitBonus ();
		Score.score = 0;

		yield return new WaitForSeconds (2);

		BoxGUI.SetActive (true);

		rb.isKinematic = true;

		audio.clip = audioClip [3];
		audio.loop = true;
		audio.Play ();

		

	}

}
