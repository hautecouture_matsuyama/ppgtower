﻿using UnityEngine;
using System.Collections;
using NendUnityPlugin.AD;

public class NendManager : MonoBehaviour {

    public static NendManager Instance;

    [SerializeField]
    NendAdBanner nendAdBanner;

    [SerializeField]
    NendAdBanner nendAdPauseRec;

#if UNITY_ANDROID
    [SerializeField]
    NendAdIcon nendAdIcon;
#endif

    void Awake()
    {
       
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }

        Debug.Log("NENDMANAGER");
    }

    //バナー
    public void NendBannerShow()
    {
        nendAdBanner.Show();
    }
    public void NendBannerHide()
    {
        nendAdBanner.Hide();
    }


    //ポーズ画面用レクタングル
    public void NendPauseRecShow()
    {
        nendAdPauseRec.Show();
    }
    public void NendPauseRecHide()
    {
        nendAdPauseRec.Hide();
    }


#if UNITY_ANDROID
    //アイコン
    public void NendIconShow()
    {
        nendAdIcon.Show();
    }
    public void NendIconHide()
    {
        nendAdIcon.Hide();
    }
#endif



	






}
