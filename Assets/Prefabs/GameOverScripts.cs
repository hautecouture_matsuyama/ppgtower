﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using NendUnityPlugin.AD;

public class GameOverScripts : MonoBehaviour {

    [SerializeField]
    private Text scoreText;

    [SerializeField]
    private Text bestText;

    [SerializeField]
    private Text NowRank;


    IEnumerator ShowInterStitial()
    {

        yield return new WaitForSeconds(Random.Range(0.2f, 0.8f));
        NendAdInterstitial.Instance.Show();
    }

	void OnEnable()
    {
        Sound.Instance.StartSound(4);

        scoreText.text = PlayerPrefs.GetInt("nowScore").ToString();

        bestText.text = PlayerPrefs.GetInt("score").ToString();

		Debug.Log ("sssss");

        if (Random.value < 0.33f)
            StartCoroutine(ShowInterStitial());

        RankingManager.Instance.GetRankingData();

        NendManager.Instance.NendBannerHide();
        NendManager.Instance.NendPauseRecShow();
        NendManager.Instance.NendIconShow();
		
    }

	void OnDisable()
	{
		NendManager.Instance.NendPauseRecHide();
	}
}
