﻿using UnityEngine;
using System.Collections;

public class CameraScript : MonoBehaviour {


    Camera _mainCamera;

    private Vector3 pos;

    [SerializeField]
    private GameObject right_lazaser;

    [SerializeField]
    private GameObject left_laser;

    [SerializeField]
    private GameObject[] Enemy;

    [SerializeField]
    private float interval;

	[SerializeField]
	private PlayerControl pl;

    private int cnt;

    public static bool enemyBonus;

    void Start()
    {
        InitializedLaser();

        if (enemyBonus)
            interval = 5f;
        else
            interval = 0.5f;

        StartCoroutine(EnemySpawn());
    }

    
    IEnumerator EnemySpawn()
    {

        yield return new WaitForSeconds(5);


        while (true)
        {
            yield return new WaitForSeconds(interval);
            if (Random.value > 0.1f)
            {

                Vector3 pos = _mainCamera.ScreenToWorldPoint(new Vector3(Random.RandomRange(20, Screen.width * 0.8f), Screen.height * 1.1f, 25f));
                Instantiate(Enemy[Random.RandomRange(0, 4)], pos, Quaternion.identity);

                if (!enemyBonus)
                {
                    if (Score.score <= 150)
                        interval = 2.5f - Mathf.Floor(Score.score / 10) / 10;
                    else
                        interval = 1f;
                }

            }

			if (!pl.aliveflg)
				break;


        }
    }
    

    void InitializedLaser()
    {
        _mainCamera = gameObject.GetComponent<Camera>();

        right_lazaser.transform.position = _mainCamera.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height*0.7f, 25));
        left_laser.transform.position = _mainCamera.ScreenToWorldPoint(new Vector3(0, Screen.height*0.7f, 25));
    }


   


}
