﻿using UnityEngine;
using System.Collections;

public class Sound : MonoBehaviour {




    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }


        HC_SoundManager.LoadSe("1", "ButtonTap");
        HC_SoundManager.LoadSe("2", "laser");
        HC_SoundManager.LoadSe("3", "muteki");

    }

    public static Sound Instance;


    public void StartSound(int no)
    {

        switch(no)
        {
            case 1:
                HC_SoundManager.PlayBgm("1");
                break;
            case 2:
                HC_SoundManager.PlayBgm("2");
                break;
            case 3:
                HC_SoundManager.PlayBgm("3");
                break;
            case 4:
                HC_SoundManager.PlayBgm("4");
                break;
        }
    }

    public void StartSE(int no)
    {
        switch (no)
        {
            case 1:
                HC_SoundManager.PlaySe("1");
                break;
            case 2:
                HC_SoundManager.PlaySe("2");
                break;
            case 3:
                HC_SoundManager.PlaySe("3");
                break;
        }

    }


    public void StopBBM()
    {
        HC_SoundManager.StopBgm();
    }
	
}
