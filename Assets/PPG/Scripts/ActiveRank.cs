﻿using UnityEngine;
using System.Collections;

public class ActiveRank : MonoBehaviour {

    private GameObject rankingPanel;

	// Use this for initialization
	void Start () {

        rankingPanel = GameObject.Find("RankingParent").transform.Find("RankingCanvas").gameObject;
	
	}
	

    public void Actives()
    {
        rankingPanel.SetActive(true);
        RankingManager.Instance.GetRankingData();
		NendManager.Instance.NendPauseRecHide ();
        NendManager.Instance.NendIconHide();
    }

	
}
