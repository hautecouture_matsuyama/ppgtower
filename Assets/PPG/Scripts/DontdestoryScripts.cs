﻿using UnityEngine;
using System.Collections;

public class DontdestoryScripts : MonoBehaviour {

    static public DontdestoryScripts instance;


    void Awake()
    {
        if(instance == null)
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }
}
