﻿using UnityEngine;
using System.Collections;

public class Linelaser : MonoBehaviour {


    [SerializeField]
    private GameObject left_laser;

    [SerializeField]
    private GameObject right_laser;

    private LineRenderer laser;

	// Use this for initialization
	void Start () {

        laser = gameObject.GetComponent<LineRenderer>();
	
	}
	
	// Update is called once per frame
	void Update () {


        laser.SetPosition(0, right_laser.transform.position);
        laser.SetPosition(1, left_laser.transform.position);
   


	
	}
}
