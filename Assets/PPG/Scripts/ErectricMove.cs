﻿using UnityEngine;
using System.Collections;

public class ErectricMove : MonoBehaviour {

    [SerializeField]
    private GameObject leftObj;

    [SerializeField]
    private GameObject rightObj;

    [SerializeField]
    private float speed;

    private GameObject target;

    private float rad;

    private Vector2 position;


	// Use this for initialization
	void Start () {

        target = leftObj;

        rad = Mathf.Atan2(
            target.transform.position.y - transform.position.y,
            target.transform.position.x - transform.position.x);
	
	}
	
	// Update is called once per frame
	void Update () {

        Move();
	
	}

    void Move()
    {
      
        if(this.transform.position.x < leftObj.transform.position.x)
        {
            Debug.Log("HIIIIIIIII");

            target = rightObj;
            rad = Mathf.Atan2(
                target.transform.position.y - transform.position.y,
                target.transform.position.x - transform.position.x);
        }

        else if (this.transform.position.x > rightObj.transform.position.x)
        {
            Debug.Log("HIIIIIIIII");

            target = rightObj;
            rad = Mathf.Atan2(
                target.transform.position.y - transform.position.y,
                target.transform.position.x - transform.position.x);
        }




        position = transform.position;

        position.x += speed * Mathf.Cos(rad);
        position.y += speed * Mathf.Sin(rad);
        
        transform.position = position;
    }
}
