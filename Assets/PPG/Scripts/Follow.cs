﻿using UnityEngine;
using System.Collections;

public class Follow : MonoBehaviour {

    private GameObject targetObject;

    private Vector2 targetPos;

    float horizontal;

    public bool flg;

    private GameObject player;


    private Rigidbody2D rigid;

    public static bool enableFlg;

    private TrailRenderer trail;

    [SerializeField]
    private Animator anim;

    [SerializeField]
    private SpriteRenderer sp;

    void Start()
    {
        trail = gameObject.GetComponent<TrailRenderer>();
        player = GameObject.Find("player").gameObject;
        rigid = this.gameObject.GetComponent<Rigidbody2D>();

        if(this.gameObject.tag == "Bubblus")
        targetObject = GameObject.Find("player").gameObject;

        if(this.gameObject.tag == "ButterCup")
        targetObject = GameObject.Find("sub_player_1(Clone)").gameObject;
        player = targetObject;
    }

    void Update()
    {
        if (enableFlg)
        {
            //trail.enabled = true;
            anim.enabled = true;
        }
        else
        {
            //trail.enabled = false;
            anim.enabled = false;
            sp.sprite = null;
        }


        if (flg)
        {
            targetPos = targetObject.transform.position;

            StartCoroutine(Horming(targetPos));
        }
        else if (!flg)
        {
            float speed = 20.0f;
            float step = Time.deltaTime * speed;
            transform.position = Vector3.MoveTowards(transform.position, player.transform.position, step);
        }

        float dist = Vector2.Distance(targetObject.transform.position, this.gameObject.transform.position);
        if(dist < 1)
        {
            flg = true;
        }
        


    }


    //追尾コルーチン（）
    IEnumerator Horming(Vector2 vec)
    {

        Vector3 theScale = targetObject.transform.localScale;
        theScale.x *= 1;
        transform.localScale = theScale;


        yield return new WaitForSeconds(0.2f);
        this.transform.position = vec;

    }

 



}
