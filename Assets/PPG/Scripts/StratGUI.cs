﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class StratGUI : MonoBehaviour {


	public Button tilt;

	public GameObject startButton;

	[SerializeField]
	private GameObject howToPanel;


	[SerializeField]
	private Animator touchAnim;

	[SerializeField]
	private Button totchButton;

	[SerializeField]
	private Button tiltButton;


	[SerializeField]
	private Animator tiltAnim;

	[SerializeField]
	private GameObject box; 


	public void ShowHowTo()
	{
		howToPanel.SetActive(true);
        NendManager.Instance.NendIconHide();
	}
		
	void Start()
	{

		if(PlayerPrefs.GetString("Init") != "true"){

			PlayerPrefs.SetString("isGravity","false");
			PlayerPrefs.SetInt("level",1);

			PlayerPrefs.SetString("Init","true");

		}

	}


	public void Option()
	{
		box.SetActive (true);
        NendManager.Instance.NendIconHide();
		if (PlayerPrefs.GetString ("isGravity") == "true") {
			tiltAnim.enabled = true;
			totchButton.image.color = Color.white;
			tiltButton.image.color = Color.red;
		}
		else
			touchAnim.enabled = true;
		totchButton.image.color = Color.red;
		tiltButton.image.color = Color.white;
	}

	public void startBotton(){
		//NendManager.Instance.NendBannerHide();
		NendManager.Instance.NendIconHide();
		SceneManager.LoadScene("Level1");
	}

	public void tiltFun(){
		PlayerPrefs.SetString("isGravity","ture");
		touchAnim.enabled = false;
		tiltAnim.enabled = true;
		totchButton.image.color = Color.white;
		tiltButton.image.color = Color.red;

	}

	public void touchFun(){
		PlayerPrefs.SetString("isGravity","false");
		touchAnim.enabled = true;
		tiltAnim.enabled = false;
		totchButton.image.color = Color.red;
		tiltButton.image.color = Color.white;

	}
	public void level1Fun(){
		PlayerPrefs.SetInt("level",1);
	}



	public void StartFun(){

		startButton.SetActive (false);
		Time.timeScale = 1;
	}
		
	public void BackFun(){
		SceneManager.LoadScene("Start");
		NendManager.Instance.NendPauseRecHide();

	}

	public void RestartFun(){
		Application.LoadLevel(Application.loadedLevel);
		Time.timeScale = 1;

		NendManager.Instance.NendPauseRecHide();
		NendManager.Instance.NendIconHide();

	}


	public void OtherGames()
	{
		Application.OpenURL("https://itunes.apple.com/us/developer/hautecouture-inc./id901721933");

	}



}
