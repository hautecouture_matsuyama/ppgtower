﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;


public class BonusManager : MonoBehaviour {


    //ログインボーナスのダイアログ
    [SerializeField]
    private GameObject bonusPanel;

    [SerializeField]
    private GameObject nameEdit;

    //ボーナス番号（1.アイテム　2.敵　3.足場）
    public static int bonusidx;



    [SerializeField]
    private GameObject[] bonusImage;


    void Start()
    {
        int cheak = PlayerPrefs.GetInt("flg");


        if (cheak==1)
            LoginCheak();
        else
            StartCoroutine(NameCheaker());

    }
    

    void LoginCheak()
    {
        string lastlogin = PlayerPrefs.GetString("lastLogin");


        for (int i = 0; i < 3; i++ )
        {
            bonusImage[i].SetActive(false);
        }


            //初回ログイン時
            if (lastlogin == string.Empty)
            {
                //ログインボーナス
                Debug.Log("ログインボーナス!!1");

                LoginBonus();
                PlayerPrefs.SetString("lastLogin", System.DateTime.Today.ToBinary().ToString());
            }
            else
            {
                System.DateTime lastLogin = System.DateTime.FromBinary(System.Convert.ToInt64(lastlogin));

                //次の日になったか？
                if (System.DateTime.Today > lastLogin)
                {
                    //ログインボーナス
                    Debug.Log("ログインボーナス!!");
                    
                    LoginBonus();

                    PlayerPrefs.SetString("lastLogin", System.DateTime.Today.ToBinary().ToString());
                }
            

            }
    }

    //初回で名前登録の画面が出ている間は表示しない(書き直したい)
    IEnumerator NameCheaker()
    {
        yield return new WaitForSeconds(0.6f);


        while(nameEdit.activeSelf)
        {
            yield return null;
        }

        yield return new WaitForSeconds(0.2f);

        LoginCheak();

    }


    //ログインボーナス
    public void LoginBonus()
    {
        bonusPanel.SetActive(true);

        bonusidx = UnityEngine.Random.RandomRange(1, 4);


        switch(bonusidx)
        {
            case 1:

                bonusImage[0].SetActive(true);
                BGLooper.potionProb = 0.85f;
                break;
            case 2:
                bonusImage[1].SetActive(true);
                CameraScript.enemyBonus = true;
                break;
            case 3:

                bonusImage[2].SetActive(true);
                AllGUI.planBonus = true;
                break;

        }

    }
    public void LoginBonusRank()
    {
        bonusidx = UnityEngine.Random.RandomRange(1, 4);


        switch (bonusidx)
        {
            case 1:

                BGLooper.potionProb = 0.85f;
                break;
            case 2:

                CameraScript.enemyBonus = true;
                break;
            case 3:

                AllGUI.planBonus = true;
                break;

        }

    }
}
