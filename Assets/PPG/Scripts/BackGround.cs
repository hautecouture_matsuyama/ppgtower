﻿using UnityEngine;
using System.Collections;

public class BackGround : MonoBehaviour {

    void Start()
    {
        // 画面右上のワールド座標をビューポートから取得
        Vector2 max = Camera.main.ViewportToWorldPoint(new Vector2(1, 1));

        // スケールを求める。
        Vector2 scale = max * 2;

        // スケールを変更。
        transform.localScale = scale;
    }

}
