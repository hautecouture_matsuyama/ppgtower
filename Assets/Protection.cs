﻿using UnityEngine;
using System.Collections;

public class Protection : MonoBehaviour {

	
    void OnTriggerEnter2D(Collider2D collider)
    {
        if(collider.gameObject.tag == "Enemy")
        {
            Destroy(collider.gameObject);
        }
    }
}
