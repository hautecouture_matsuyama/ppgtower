﻿using UnityEngine;
using System.Collections;

public class ActiveSartButton : MonoBehaviour {


    void OnEnable()
    {
        NendManager.Instance.NendBannerHide();
        NendManager.Instance.NendPauseRecShow();
    }

    void OnDisable()
    {
        NendManager.Instance.NendBannerShow();
        NendManager.Instance.NendPauseRecHide();
    }


}
