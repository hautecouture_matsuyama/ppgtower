﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CountPanelScripts : MonoBehaviour {

	[SerializeField]
	private Text readyText;

	void OnEnable()
	{
		StartCoroutine (IsReady ());

	}

	IEnumerator IsReady()
	{
		yield return new  WaitForSeconds (0.5f);
		readyText.text = "Ready.";
		yield return new  WaitForSeconds (0.5f);
		readyText.text = "Ready..";
		yield return new  WaitForSeconds (0.5f);
		readyText.text = "GO!!!!";
		readyText.fontSize = 150;

	}

}
