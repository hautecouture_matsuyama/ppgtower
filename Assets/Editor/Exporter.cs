﻿using UnityEngine;
using System.Collections;
using UnityEditor;

public class Exporter : MonoBehaviour {

	[MenuItem("Assets/ExportWithSettings")]
	static void Export () {
		AssetDatabase.ExportPackage ("Assets/", "EFTT.unitypackage", 
ExportPackageOptions.Interactive | ExportPackageOptions.Recurse | ExportPackageOptions.IncludeLibraryAssets | ExportPackageOptions.IncludeDependencies);
	}
}
