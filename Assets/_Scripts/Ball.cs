﻿using UnityEngine;
using System.Collections;

public class Ball : MonoBehaviour {


    [SerializeField]
    private float jumpSpeed;

    [SerializeField]
    private bool isJumping;

    private int count = 0;

    private Rigidbody2D myRigidbbody;


	// Use this for initialization
	void Start () {

        myRigidbbody = gameObject.GetComponent<Rigidbody2D>();
	
	}
	
	// Update is called once per frame
	void Update () {

        if(Input.GetButtonDown("Jump") && !isJumping && count <= 1)
        {
            myRigidbbody.velocity = Vector3.up * jumpSpeed;
            count++; 
        }

	}

    void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.CompareTag("Floor"))
        {
            count = 0;
        }
    }
}
