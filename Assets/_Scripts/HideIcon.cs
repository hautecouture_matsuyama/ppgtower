﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HideIcon : MonoBehaviour {


    private void OnEnable()
    {
        StartCoroutine(Hide());
    }

    private void OnDisable()
    {
        NendManager.Instance.NendIconShow();
    }

    IEnumerator Hide()
    {
        yield return new WaitForSeconds(0.01f);
        NendManager.Instance.NendIconHide();
    }


}
