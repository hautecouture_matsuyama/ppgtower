﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CloseButton : MonoBehaviour {

    [SerializeField]
    private GameObject _closeObject;


    public void Close()
    {
        _closeObject.SetActive(false);
        NendManager.Instance.NendIconShow();

        if(SceneManager.GetActiveScene().name == "Level1")
        {
            NendManager.Instance.NendPauseRecShow();
        }

    }

}
